<?php

use LaravelHaml\Twig\Extensions\Foundation;

return [
    'twig'          => [
        'auto_reload'   => true,
        'cache'         => storage_path('framework/views/twig'),
        'optimizations' => Twig_NodeVisitor_Optimizer::OPTIMIZE_ALL,
    ],
    'assumeAllHaml' => true,
    'extensions'    => [
        Foundation::class
    ],
    'facades'       => [
        Illuminate\Support\Facades\Crypt::class,
        Illuminate\Support\Facades\Hash::class,
        Illuminate\Support\Facades\Lang::class,
        Illuminate\Support\Facades\Password::class,
        Illuminate\Support\Facades\Schema::class,
        Illuminate\Support\Facades\Session::class,
        Illuminate\Support\Facades\URL::class,
        Illuminate\Support\Facades\View::class, 
        Auth::class
    ]
];