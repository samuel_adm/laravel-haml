<?php

namespace LaravelHaml;

use Illuminate\Support\Facades\Facade;
use LaravelHaml\Twig\Compiler;
use LaravelHaml\Twig\Engine;
use LaravelHaml\Twig\FileViewFinderLoader as AdapterLoader;
use LaravelHaml\Twig\HamlLoader;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use MtHaml\Environment;
use MtHaml\Support\Twig\Extension;

/**
 * Class ServiceProvider
 *
 * @package LaravelHaml
 */
class ServiceProvider extends BaseServiceProvider
{
    /** {@inheritDoc} */
    public function register()
    {
        $this->app->bindIf('haml', function () { return new Environment('twig', ['enable_escaper' => false]); });
        $this->app->bindIf('haml.extensions', function ($app) {
            $enabledExtensions   = $app['config']['haml.extensions'] ?: [];
            $enabledExtensions[] = new Extension($app['haml']);

            return $enabledExtensions;
        });
        $this->app->bindIf('haml.facades', function ($app) {
            $registeredFacades = $app['config']['haml.facades'];

            foreach ($registeredFacades as $registeredFacade) {
                if (!is_subclass_of($registeredFacade, Facade::class)) {
                    throw new \InvalidArgumentException("Class '{$registeredFacade}' does not extends Illuminate\\Support\\Facades\\Facade");
                }
            }

            return $registeredFacades;
        });
        $this->app->bindIf('twig.templates', function () { return []; });
        $this->app->bindIf('twig.loader.array', function ($app) { return new \Twig_Loader_Array($app['twig.templates']); });
        $this->app->bindIf('twig.loader.adapter', function ($app) { return new AdapterLoader($app['view']->getFinder()); });
        $this->app->bindIf('twig.loader.chain', function ($app) {
            return new \Twig_Loader_Chain([
                $app['twig.loader.array'],
                $app['twig.loader.adapter']
            ]);
        });
        $this->app->bindIf('twig.loader', function ($app) {
            return new HamlLoader(
                $app['haml'],
                $app['twig.loader.chain'],
                $app['config']['haml.assumeAllHaml']
            );
        });
        $this->app->bindIf('twig', function ($app) {
            $twig = new \Twig_Environment(
                $app['twig.loader'],
                $app['config']['haml.twig']
            );

            foreach ($app['haml.extensions'] as $extension) {
                if ($extension instanceof \Twig_Extension) {
                    $twig->addExtension($extension);
                } elseif (is_callable($extension)) {
                    $twig->addExtension(call_user_func_array($extension, [$app]));
                } elseif (is_string($extension)) {
                    $extension = $app->make($extension);

                    if ($extension instanceof \Twig_Extension) {
                        $twig->addExtension($extension);
                    }
                }
            }

            if ($app['config']['debug']) {
                $twig->addExtension(new \Twig_Extension_Debug());
            }

            foreach ($app['haml.facades'] as $facade) {
                $refl = new \ReflectionClass($facade);
                $key  = 'haml.facades.'.$facade;

                $app->singleton($key, function () use ($facade) { return call_user_func($facade.'::getFacadeRoot'); });
                $twig->addGlobal($refl->getShortName(), $app[$key]);
            }

            return $twig;
        });
        $this->app->bindIf('twig.compiler', function ($app) { return new Compiler($app['twig']); });
        $this->app->bindIf('twig.engine', function ($app) { return new Engine($app['twig.compiler']); });
        $this->mergeConfigFrom(__DIR__.'/../config/haml.php', 'haml');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/haml.php' => config_path('haml.php')
        ]);

        $this->app['view']->addExtension(
            'haml',
            'twig',
            function () {
                return $this->app['twig.engine'];
            }
        );
    }

    /** {@inheritDoc} */
    public function provides()
    {
        return [
            'haml',
            'haml.extensions',
            'twig',
            'twig.templates',
            'twig.loader.array',
            'twig.loader.adapter',
            'twig.loader.chain',
            'twig.loader',
            'twig.compiler',
            'twig.engine',
        ];
    }
}