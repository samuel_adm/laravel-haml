<?php

namespace LaravelHaml\Twig;

use Illuminate\View\Compilers\CompilerInterface;

/**
 * Class Compiler
 *
 * @package LaravelHaml\Twig
 */
class Compiler implements CompilerInterface
{
    /** @var \Twig_Environment */
    protected $twig;
    /** @var \Twig_CacheInterface */
    protected $cache;

    public function __construct(\Twig_Environment $twig)
    {
        $this->twig  = $twig;
        $this->cache = $this->twig->getCache(false);
    }

    /**
     * Get the used environment
     *
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /** {@inheritDoc} */
    public function compile($path)
    {
        $this->twig->loadTemplate($path);
    }

    /** {@inheritDoc} */
    public function getCompiledPath($path)
    {
        return $this->twig->getCacheFilename($path);
    }

    /** {@inheritDoc} */
    public function isExpired($path)
    {
        $compiledPath = $this->getCompiledPath($path);

        if (!file_exists($compiledPath)) {
            return true;
        }

        return $this->twig->isTemplateFresh($path, filemtime($compiledPath));
    }
}