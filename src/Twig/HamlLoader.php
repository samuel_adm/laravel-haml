<?php

namespace LaravelHaml\Twig;

use MtHaml\Environment;
use MtHaml\Support\Twig\Loader as BaseHamlLoader;

/**
 * Class HamlLoader
 *
 * @package LaravelHaml\Twig
 */
class HamlLoader extends BaseHamlLoader
{
    /** @var bool */
    protected $alwaysAssumeHaml;

    public function __construct(Environment $env, \Twig_LoaderInterface $loader, $alwaysAssumeHaml = false)
    {
        parent::__construct($env, $loader);

        $this->alwaysAssumeHaml = $alwaysAssumeHaml;
    }

    /** {@inheritDoc} */
    public function getSource($name)
    {
        if ($this->alwaysAssumeHaml) {
            $source = $this->loader->getSource($name);

            if (preg_match('#^\s*{%\s*haml\s*%}#', $source, $match)) {
                $padding = str_repeat(' ', strlen($match[0]));
                $source  = $padding.substr($source, strlen($match[0]));
                $source  = $this->env->compileString($source, $name);
            } else {
                $source = $this->env->compileString($source, $name);
            }

            return $source;
        }

        return parent::getSource($name);
    }
}