<?php

namespace LaravelHaml\Twig;

use Illuminate\View\FileViewFinder;

/**
 * Class Loader
 *
 * @package LaravelHaml\Twig
 */
class FileViewFinderLoader extends \Twig_Loader_Filesystem
{
    /** @var FileViewFinder */
    protected $viewFinder;
    /** @var array */
    protected $cache = [];

    public function __construct(FileViewFinder $viewFinder)
    {
        parent::__construct($viewFinder->getPaths());

        $this->viewFinder = $viewFinder;
    }

    /**
     * Get the view finder
     *
     * @return \Illuminate\View\FileViewFinder
     */
    public function getFinder()
    {
        return $this->viewFinder;
    }

    /** {@inheritDoc} */
    protected function findTemplate($name)
    {
        if ($this->viewFinder->getFilesystem()->exists($name)) {
            return $name;
        }

        return $this->viewFinder->find($name);
    }
}