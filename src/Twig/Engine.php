<?php

namespace LaravelHaml\Twig;

use Illuminate\View\Engines\EngineInterface;

/**
 * Class Engine
 *
 * @package LaravelHaml\Twig
 */
class Engine implements EngineInterface
{
    /** @var Compiler */
    protected $compiler;

    public function __construct(Compiler $compiler)
    {
        $this->compiler = $compiler;
    }

    /** {@inheritDoc} */
    public function get($path, array $data = [])
    {
        return $this->compiler->getTwig()->render($path, $data);
    }
}