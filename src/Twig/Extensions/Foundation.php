<?php

namespace LaravelHaml\Twig\Extensions;

/**
 * Class Foundation
 *
 * @package LaravelHaml\Twig\Extensions
 */
class Foundation extends \Twig_Extension
{
    /** {@inheritDoc} */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('asset', 'asset', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('action', 'action', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('csrf_token', 'csrf_token', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('csrf_field', 'csrf_field', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('config', 'config', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('session', 'session', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('url', 'url', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('route', 'route', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('route_has', 'route_has', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('secure_url', 'secure_url', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('secure_asset', 'secure_asset', ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('old', 'old', ['is_safe' => ['html']])
        ];
    }

    /** {@inheritDoc} */
    public function getName()
    {
        return 'LaravelHamlFoundationExtension';
    }
}
