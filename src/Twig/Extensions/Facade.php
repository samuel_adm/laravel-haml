<?php

namespace LaravelHaml\Twig\Extensions;

use Illuminate\Container\Container as Application;

/**
 * Class Facade
 *
 * @package LaravelHaml\Twig\Extensions
 */
class Facade extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /** @var array */
    protected $facades;

    public function __construct(Application $app)
    {
        $this->facades = $app['config']['haml.facades'];
    }

    /** {@inheritDoc} */
    public function getName()
    {
        return 'LaravelHamlFacadeExtension';
    }
}